using FluentAssertions;
using FluentAssertions.Execution;
using NUnit.Framework;

namespace Problems.BinarySearchStudyPlan;

//704: https://leetcode.com/problems/binary-search/ 
public class BinarySearch
{
    public int Search(int[] nums, int target)
    {
        var left = 0;
        var right = nums.Length - 1;

        while (right >= left)
        {
            var mid = (left + right) >> 1;
            var current = nums[mid];

            if (current == target)
            {
                return mid;
            }

            if (current > target)
            {
                right = mid - 1;
            }
            else
            {
                left = mid + 1;
            }
        }

        return -1;
    }
    
    [Test]
    public void Tests()
    {
        using (new AssertionScope())
        {
            Search(new[] { -1, 0, 3, 5, 9, 12 }, 9).Should().Be(4);
            Search(new[] { 1, 2 }, 2).Should().Be(1);
            Search(new[] { 1, 2 }, 1).Should().Be(0);
            Search(new[] { 1 }, 1).Should().Be(0);
            Search(new[] { 1 }, 2).Should().Be(-1);
        }
    }
}