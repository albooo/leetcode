using System;
using FluentAssertions;
using FluentAssertions.Execution;
using NUnit.Framework;

namespace Problems.BinarySearchStudyPlan;

//374: https://leetcode.com/problems/guess-number-higher-or-lower/ 
public class GuessNumberHigherOrLower
{
    private readonly int _pick;

    public GuessNumberHigherOrLower(int pick)
    {
        _pick = pick;
    }

    private int Guess(int number) => _pick.CompareTo(number);

    public int GuessNumber(int n)
    {
        var left = 1;
        var right = n;

        while (right >= left)
        {
            var mid = left + (right - left) / 2;
            var guessResult = Guess(mid);

            switch (guessResult)
            {
                case 0:
                    return mid;
                case -1:
                    right = mid - 1;
                    break;
                case 1:
                    left = mid + 1;
                    break;
                default: throw new NotSupportedException();
            }
        }

        return 0;
    }
}

public class Tests
{
    [Test]
    public void Test()
    {
        using (new AssertionScope())
        {
            new GuessNumberHigherOrLower(1702766719).GuessNumber(2126753390).Should().Be(1702766719);
            new GuessNumberHigherOrLower(1).GuessNumber(1).Should().Be(1);
            new GuessNumberHigherOrLower(1).GuessNumber(2).Should().Be(1);
        }
    }
}