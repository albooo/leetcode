using FluentAssertions;
using FluentAssertions.Execution;
using NUnit.Framework;

namespace Problems.BinarySearchStudyPlan;

//35: https://leetcode.com/problems/search-insert-position/
public class SearchInsertPosition
{
    public int SearchInsert(int[] nums, int target)
    {
        var left = 0;
        var right = nums.Length;
        
        while (left < right) {
            var mid = left + (right - left) / 2;
            if (nums[mid] >= target) right = mid;
            else left = mid + 1;
        }
        
        return left;
    }
    
    [Test]
    public void Tests()
    {
        using (new AssertionScope())
        {
            SearchInsert(new[] { -1, 0, 3, 5, 9, 12 }, 9).Should().Be(4);
            SearchInsert(new[] { 1, 2 }, 2).Should().Be(1);
            SearchInsert(new[] { 1, 2 }, 1).Should().Be(0);
            SearchInsert(new[] { 1 }, 1).Should().Be(0);
            SearchInsert(new[] { 1, 2, 4 }, 3).Should().Be(2);
            SearchInsert(new[] { 1 }, 2).Should().Be(1);
            SearchInsert(new[] { 1, 3, 5, 6 }, 5).Should().Be(2);
            SearchInsert(new[] { 1, 3, 5, 6 }, 2).Should().Be(1);
            SearchInsert(new[] { 1, 3, 5, 6 }, 7).Should().Be(4);
            SearchInsert(new[] { 1, 3 }, 0).Should().Be(0);
            SearchInsert(new[] { 3, 5, 7, 9, 10 }, 8).Should().Be(3);
        }
    }
}